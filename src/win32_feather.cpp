#define WIN32_LEAN_AND_MEAN
#define VC_EXTRALEAN
#include <windows.h>
#include <gl/GL.h>

#include "log.h"

static HDC deviceContext;

LRESULT CALLBACK win32WindowProcedure(
	HWND window,
	UINT message,
	WPARAM wParam,
	LPARAM lParam)
{
	LRESULT result = 1;
	switch (message)
	{
		case WM_QUIT:
		{
			DestroyWindow(window);
		}
		break;

		case WM_DESTROY:
		{
			PostQuitMessage(0);
		}
		break;

		case WM_SIZE:
		{
			log_info("Resize event");
		}
		break;

		case WM_CHAR:
		{
			log_info("char:");
		}
		break;

		case WM_PAINT:
		{
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			SwapBuffers(deviceContext);
		}

		default:
		result = DefWindowProc(window, message, wParam, lParam);
	}
	return result;
}


int WINAPI WinMain(
	HINSTANCE instance,
	HINSTANCE PrevInstance,
	LPSTR commandLine,
	int showCode)
{
	log_init();

	WNDCLASSEX windowClass = {0};
	HWND window = {0};
	HGLRC glRenderContext;


	windowClass.cbSize = sizeof(WNDCLASSEX);
	windowClass.style = CS_OWNDC;
	windowClass.lpfnWndProc = win32WindowProcedure;
	windowClass.hCursor = LoadCursor(0, IDC_ARROW);
	windowClass.lpszClassName = "FeatherWindowClass";

	if (!RegisterClassEx(&windowClass))
		return 1;

	RECT tempClientRect = {0, 0, 640, 480};
	AdjustWindowRectEx(&tempClientRect, WS_OVERLAPPEDWINDOW, false, 0);

	window = CreateWindowEx(
		0,
		"FeatherWindowCLass",
		"Feather",
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		tempClientRect.right - tempClientRect.left,
		tempClientRect.bottom - tempClientRect.top,
		0,
		0,
		instance,
		0);

	if (!window)
		return 1;

	deviceContext = GetDC(window);
	if (!deviceContext)
		return 1;

	PIXELFORMATDESCRIPTOR pfd = 
	{
		sizeof(PIXELFORMATDESCRIPTOR),
		1,
		PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL |PFD_DOUBLEBUFFER,
		PFD_TYPE_RGBA,
		32,
		0, 0, 0, 0, 0, 0,
		0,
		0,
		0,
		0, 0, 0, 0,
		24,
		8,
		0,
		PFD_MAIN_PLANE,
		0,
		0, 0, 0
	};

	int pixelFormat = ChoosePixelFormat(deviceContext, &pfd);
	if (!pixelFormat)
		return 1;

	if (!SetPixelFormat(deviceContext, pixelFormat, &pfd))
		return 1;

	glRenderContext = wglCreateContext(deviceContext);

	if (!glRenderContext)
		return 1;

	wglMakeCurrent(deviceContext, glRenderContext);

	ShowWindow(window, showCode);
	UpdateWindow(window);

	glClearColor(1.0, 0.0, 0.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	SwapBuffers(deviceContext);

	MSG message;

	while (GetMessage(&message, 0, 0, 0) > 0)
	{
		TranslateMessage(&message);
		DispatchMessage(&message);
	}
	return 0;
}

int main(int argc, char **argv)
{
	return WinMain(GetModuleHandle(0), 0, GetCommandLine(), SW_SHOW);
}

#include "log.cpp"
