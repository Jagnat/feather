#include "feather_data.h"
#include "feather_funcs.h"

void bufferInit(Buffer *buffer, BufferPool *pool)
{
	buffer->first = bufferpoolAlloc(pool);
	buffer->last = bufferpoolAlloc(pool);
	buffer->first->prev = 0;
	buffer->first->next = buffer->last;
	buffer->last->prev = buffer->first;
	buffer->last->next = 0;
}

void bufferInsertAfter(Buffer *buffer, BufferLine *line, BufferLine *insert)
{
	insert->prev = line;
	insert->next = line->next;
	if (line->next == 0)
		buffer->last = insert;
	else
		line->next->prev = insert;
	line->next = insert;
}

void bufferInsertBefore(Buffer *buffer, BufferLine *line, BufferLine *insert)
{
	insert->prev = line->prev;
	insert->next = line;
	if (line->prev == 0)
		buffer->first = insert;
	else
		line->prev->next = insert;
	line->prev = insert;
}

void bufferpoolInit(BufferPool *pool, int num)
{
	pool->num = num;
	pool->lines = (BufferLine*)calloc(1, sizeof(BufferLine) * num);
	pool->freelist = (uint8*)calloc(1, sizeof(uint8) * num);
}

BufferLine *bufferpoolAlloc(BufferPool *pool)
{
	assert(pool->num > 0);
	for (int i = 0; i < pool->num; ++i)
	{
		if (pool->freelist[i] == 0)
		{
			pool->freelist[i] = 255;		
			return &pool->lines[i];
		}
	}
	return 0;
}

void bufferpoolFree(BufferPool *pool, BufferLine *line)
{
	assert(line >= pool->lines && line < pool->lines + pool->num);
	int index = line - pool->lines;
	pool->freelist[index] = 0;
	pool->lines[index] = {0};
}
