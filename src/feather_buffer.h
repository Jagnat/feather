#pragma once
#ifndef _FEATHER_BUFFER_H_
#define _FEATHER_BUFFER_H_

// TODO: this shouldn't exist
#define MAX_COLUMNS 128
struct BufferLine
{
	int8 line[MAX_COLUMNS];
	BufferLine *prev, *next;
};

struct Buffer
{
	BufferLine *first, *last;
};

struct BufferPool
{
	BufferLine *lines;
	uint8 *freelist;
	int num;
};

void bufferpoolInit(BufferPool *pool, int num);
// NOTE: check for null
BufferLine *bufferpoolAlloc(BufferPool *pool);
void bufferpoolFree(BufferPool *pool, BufferLine *line);

void bufferInit(Buffer *buffer, BufferPool *pool);
void bufferInsertAfter(Buffer *buffer, BufferLine *line, BufferLine *insert);
void bufferInsertBefore(Buffer *buffer, BufferLine *line, BufferLine *insert);

#endif
